package com.inkss.hb.orderInfo.mapper;

import com.inkss.hb.orderInfo.pojo.OrderInfo;

import java.util.List;

public interface OrderInfoMapper {

    void insertOrderInfo(OrderInfo orderInfo);

    void updateOrderInfo(OrderInfo orderInfo);

    List<OrderInfo> selectByOrderState(String orderState);

    OrderInfo selectByOrderId(String orderId);
}
