package com.inkss.hb.orderInfo.service;

import com.inkss.hb.orderInfo.mapper.OrderInfoMapper;
import com.inkss.hb.orderInfo.pojo.OrderInfo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class OrderInfoServiceImpl implements OrderInfoService{

    private static SqlSessionFactory sqlSessionFactory;
    private SqlSession sqlSession = null;
    private OrderInfoMapper orderInfoMapper=null;

    static {
        String resource = "SqlMapConfig.xml";
        try {
            InputStream inputStream = Resources.getResourceAsStream(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int insertOrderInfo(OrderInfo orderInfo) {
        try {
            sqlSession = sqlSessionFactory.openSession();
            orderInfoMapper  = sqlSession.getMapper(OrderInfoMapper.class);
            orderInfoMapper.insertOrderInfo(orderInfo);
            sqlSession.commit();
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public int updateOrderInfo(OrderInfo orderInfo) {
        try {
            sqlSession = sqlSessionFactory.openSession();
            orderInfoMapper = sqlSession.getMapper(OrderInfoMapper.class);
            orderInfoMapper.updateOrderInfo(orderInfo);
            sqlSession.commit();
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public List<OrderInfo> selectByOrderState(String orderState) {
        try {
            sqlSession = sqlSessionFactory.openSession();
            orderInfoMapper = sqlSession.getMapper(OrderInfoMapper.class);
            return orderInfoMapper.selectByOrderState(orderState);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public OrderInfo selectByOrderId(String orderId) {
        try {
            sqlSession = sqlSessionFactory.openSession();
            orderInfoMapper = sqlSession.getMapper(OrderInfoMapper.class);
            return orderInfoMapper.selectByOrderId(orderId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            sqlSession.close();
        }
    }


}
