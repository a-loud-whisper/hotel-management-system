package com.inkss.hb.orderInfo.service;

import com.inkss.hb.orderInfo.pojo.OrderInfo;

import java.util.List;

public interface OrderInfoService {

    int insertOrderInfo(OrderInfo orderInfo);

    int updateOrderInfo(OrderInfo orderInfo);

    List<OrderInfo> selectByOrderState(String orderState);

    OrderInfo selectByOrderId(String orderId);

}
