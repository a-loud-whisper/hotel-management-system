package com.inkss.hb.orderInfo.controller;

import com.google.gson.Gson;
import com.inkss.hb.common.PojotoGson;
import com.inkss.hb.orderInfo.pojo.OrderInfo;
import com.inkss.hb.orderInfo.service.OrderInfoService;
import com.inkss.hb.orderInfo.service.OrderInfoServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Controller
@RequestMapping(value = "OrderInfoController", produces = {"text/html;charset=UTF-8;", "application/json;charset=UTF-8;"})
public class OrderInfoController {

    private OrderInfoService service = new OrderInfoServiceImpl();

    @RequestMapping("inOrderTable.do")
    @ResponseBody
    public String logInfoTable(HttpServletRequest request, HttpServletResponse response) throws IOException {

        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");

        String orderId = request.getParameter("orderId");  //1
        String orderName = request.getParameter("orderName");  //2
        String orderPhone = request.getParameter("orderPhone"); //3
        String orderIDcard = request.getParameter("orderIDcard");  //4
        String typeName = request.getParameter("typeName"); //5
        String typeId = request.getParameter("typeId"); //5
        String arrireDate = request.getParameter("arrireDate"); //6
        String leaveDate = request.getParameter("leaveDate"); //7
        String orderState = request.getParameter("orderState"); //8
        String checkNum = request.getParameter("checkNum"); //9
        String roomId = ""; //10
        String price = request.getParameter("price"); //11
        String checkPrice = request.getParameter("checkPrice"); //12
        String discountReason = request.getParameter("discountReason"); //14
        String addBed = request.getParameter("addBed"); //15
        String addBedPrice = request.getParameter("addBedPrice"); //16
        String orderMoney = request.getParameter("orderMoney"); //17
        String remark = request.getParameter("remark"); //18
        String operatorId = request.getParameter("operatorId"); //19
        int make = Integer.parseInt(request.getParameter("make")); // 20 标志啊

        int discount;

        try { //对折扣值为空的处理
            discount = Integer.parseInt(request.getParameter("discount")); //13
        } catch (NumberFormatException e) {
            discount = 0;
        }

        OrderInfo orderInfo = new OrderInfo(typeId,orderId, orderName, orderPhone, orderIDcard, typeName, arrireDate, leaveDate, orderState, checkNum, roomId, price, checkPrice, discount, discountReason, addBed, addBedPrice, orderMoney, remark, operatorId);
        int code = -1; //返回的状态
        if (make == 1) { //1.新增
            code = service.insertOrderInfo(orderInfo);
        } else if (make == 2) { //修改
            code = service.updateOrderInfo(orderInfo);
        }


        //make=1 -> 1:插入成功 0：存在同名项 -1:插入失败
        //make=2 -> 1:修改成功 -1;修改失败
        Gson gson = new Gson();
        return gson.toJson(code);
    }


    @RequestMapping("selectByOrderStateYuDing.do")
    @ResponseBody
    public String selectByOrderStateYuDing(){
        return selectState("预定");
    }

    @RequestMapping("selectByOrderStateRuZhu.do")
    @ResponseBody
    public String selectByOrderStateRuZhu(){
        return selectState("入住");
    }

    public String selectState(String state){
        String code = "0"; //状态码
        String msg = "数据查询正常"; //状态信息
        String count; //数据总数
        List<OrderInfo> list=service.selectByOrderState(state);
        PojotoGson pojotoGson = new PojotoGson(code, msg, String.valueOf(list.size()), list);
        Gson gson = new Gson();
        return gson.toJson(pojotoGson);
    }

    @RequestMapping("selectByOrderId.do")
    @ResponseBody
    public String SelectByOrderId(String orderId){
        Gson gson = new Gson();
        return gson.toJson(service.selectByOrderId(orderId));
    }


}
